### What-is-archive

This project is an archive of TPH "what-is-*x*" channels. These channels are used to demonstrate in layman's terms
the advantages of a particular technology. It's used in our spotlight process wherein we shine the light on this 
channel and provide a discussion channel to go with it if a relevant one does not already exist. 

The original idea here was to point out the advantages of lesser known technology. The technology spotlight will 
probably continue to spotlight lesser known pieces of tech, or extremely useful pieces of tech. This means that a
"what is python" technology spotlight may not ever happen. A "what is python" document is still a welcome addition here,
though.